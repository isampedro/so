//
// Created by khandoras on 6/3/20.
//
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <stdio.h>
#include <time.h>


int main( void ){
    time_t rawtime;
    struct tm * timeinfo;

    srand(time(NULL));
    // we are in child

    printf("We are on another file!\n");
    int secs;
    printf("Waiting %d seconds for child %d\n", (secs = rand() % 10 + 1), getpid() );
    sleep(secs);
    return 0;
}
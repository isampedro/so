//
// Created by khandoras on 4/3/20.
//

#include <dirent.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>

// FUNCTIONS DECLARATION
int show_dir_info_rec( char* );
void print_directory( const char*, const char*, const char* );
void get_new_directory( char*, const char*, unsigned long, const char* );
void get_file_type( char*, unsigned char );
void dir_depth_to_string( char*, int );
int get_dir_depth( const char*, unsigned long );
int valid_directory(  const char* );

#define FALSE 0
#define TRUE !FALSE
#define SUCCESS 0
#define FAILURE !SUCCESS

int show_dir_info( char* directory ){
    return show_dir_info_rec(directory);
}

int show_dir_info_rec( char* directory )
{
    DIR* dir = opendir(directory); // The current directory

    unsigned long dir_len = strlen( directory); // The length of the current directory

    struct dirent* dirInfo; // Relevant information of the current directory

    char file_type[] = {0,0}; // Current directory's type
    char new_directory[2000]; // The directory that will be created if the current one is not the last directory on the branch

    int dir_depth = get_dir_depth(directory, dir_len);
    char dir_depth_string[100] = {0}; // The depth of the current directory but indicated with \t in a string

    dir_depth_to_string(dir_depth_string, dir_depth);

    if( dir != NULL ) {
        // If readdir(dir) is NULL, it means the directory is empty
        while((dirInfo = readdir(dir)) != NULL ) {
            if( valid_directory(dirInfo->d_name) ) {
                get_file_type( file_type, dirInfo->d_type );

                print_directory( file_type, dir_depth_string, dirInfo->d_name );

                get_new_directory( new_directory, directory, dir_len, dirInfo->d_name );
                show_dir_info_rec(new_directory);
                *(new_directory + dir_len) = 0;
            }
        }
        closedir(dir); // Every opened directory must be closed when finished with it
    } else{
        return FAILURE;
    }
    return SUCCESS;
}

int valid_directory(  const char* dir_name ){
    if( *dir_name != '.' && *(dir_name + 1) != 0 && *(dir_name + 1) != '.' ) {
        return TRUE;
    }
    return FALSE;
}

int get_dir_depth( const char* directory, unsigned long dir_len ){
    int dir_depth = 0;
    for( unsigned long i = 0; i < dir_len; i++ ){
        if( *(directory + i) == '/')
            dir_depth++;
    }

    return dir_depth;
}

void dir_depth_to_string( char* string, int dir_depth ){
    for( int i = 0; i < dir_depth; i++ ){
        string[i] = '\t';
    }
}

void get_file_type( char* file_type, unsigned char d_type ){
    switch(d_type){
        case DT_REG: file_type[0] = 'f'; // This is a file
            break;
        case DT_DIR: file_type[0] = 'd'; // This is a directory
            break;
        case DT_FIFO: file_type[0] = 'p'; // This is a named pipe
            break;
        case DT_SOCK: file_type[0] = 's'; // This is a UNIX domain socket
            break;
        case DT_CHR: file_type[0] = 'c'; // This is a character device
            break;
        case DT_BLK: file_type[0] = 'b'; // This is a block
            break;
        case DT_LNK: file_type[0] = 'l'; // This is a symbolic link
            break;
        default: file_type[0] = 'u'; // The file type couldn't been determined
    }
}

void print_directory( const char* file_type, const char* dir_depth_string, const char* dir_name ){
    fprintf( stdout, "%s%s%s\n", file_type, dir_depth_string, dir_name );
}

void get_new_directory( char* new_directory, const char* directory, unsigned long dir_len, const char* sub_directory ){
    strcpy(new_directory, directory);
    *(new_directory + dir_len) = '/';
    strcpy(new_directory + dir_len + 1, sub_directory );
}
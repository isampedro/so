#include <stdio.h>
#include <stdlib.h>
int show_dir_info( char* dir );
void useless_function( void );

int main( int argc, char** argv ) {
    /* EJ 1
    if( argc != 2 ){
        fprintf( stderr, "ERROR: You need to call the program with a directory in its arguments (e.g.: ./program /home).\n");
        exit(1);
    }
    if( show_dir_info(argv[1])){
        fprintf( stderr, "ERROR: An error occurred while opening the given directory. The problem may be related with the directory's name or there may be a chance that the given path isn't a directory.\n");
        exit(2);
    }
*/

    useless_function();
    return 0;
}

//
// Created by khandoras on 6/3/20.
//

#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <values.h>

void useless_function( void ){
    int pid[10];
    char* argv[2] = {0};

    for( int i = 0; i < 10; i++ ){
        if( (pid[i] = fork()) == 0 ){
            execv("../childs", argv );
        }
        else{
            printf("Child %d has been set\n", pid[i] );
        }
    }

    /* Ej 4
    while( 1 ){
    }
    */

    //waitpid((pid_t)-1, NULL, WCONTINUED );
    printf("We are done!\n");
}